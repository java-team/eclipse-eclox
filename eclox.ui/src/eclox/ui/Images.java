/*******************************************************************************
 * Copyright (C) 2006-2007, 2013, Guillaume Brocker
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Guillaume Brocker - Initial API and implementation
 *
 ******************************************************************************/ 

package eclox.ui;

public interface Images {

	static String DOXYFILE_WIZARD	= "icons/doxyfile_wiz.gif";
	static String CLEAR_CONSOLE		= "icons/clear_console.gif";
	static String ERASE				= "icons/erase.png";
	static String LOCK_CONSOLE		= "icons/lock_console.gif";
	static String TERMINATE			= "icons/terminate.gif";
	static String REMOVE			= "icons/remove_console.gif";
	
}
